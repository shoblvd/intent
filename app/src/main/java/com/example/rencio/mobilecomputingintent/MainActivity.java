package com.example.rencio.mobilecomputingintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static android.support.v7.appcompat.R.styleable.View;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button mButton;
        mButton = (Button) findViewById(R.id.button);
        final Intent i = new Intent(this, Main2Activity.class);
        mButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String consonant = "";
                String answer = "";
                Editable text;
                EditText editText;
                editText = (EditText) findViewById(R.id.editText);
                text = editText.getText();
                String[] temp = text.toString().split("(?!^)");
                for (int x = 0; x < temp.length; x++) {
                    if (temp[x].equals("a") || temp[x].equals("A") || temp[x].equals("e") || temp[x].equals("E") || temp[x].equals("i") || temp[x].equals("I") || temp[x].equals("o") || temp[x].equals("O") || temp[x].equals("u") || temp[x].equals("U")) {
                    }
                    else
                    {
                        consonant = consonant + temp[x];
                    }
                }
                consonant=consonant.toLowerCase();
                int[] letterCount = new int[26];
                for (int i = 0; i < consonant.length(); i++) {
                    int chr = consonant.charAt(i);
                    if (chr > 123 || chr < 97) continue;
                    letterCount[chr - 97]++;
                }
                for (int i = 0; i < 26; i++)
                    if (letterCount[i] > 0) {
                        char chr = (char) (i + 97);
                        answer=answer + chr + " = " + letterCount[i] + "\n";
                }
                i.putExtra(EXTRA_MESSAGE, answer);
                startActivity(i);
            }
        });
    }
}
